{% docs __overview__ %}

## Data Documentation for Ebury

### Overview

  - **Why dbt:**
    - dbt is a command line tool that allows us to transform our data directly in the Data Warehouse, as well as to test data quality by including tests in our models, and to determine the order of model execution - not explicitly.
    - dbt will build a DAG based on the interdependencies between the models (i.e a model specified in a ref function will be recognized as a predecessor of the current model). Consequently, the models will be executed in the correct order specified by the DAG - through the recognition of the ref functions, without having to indicate further directions.
    - dbt is based on SQL, and is aligned with the modern BI architecture, whereby cloud-based services represent the underlying infrastructure and modularity is its core principle. Additionally, dbt can materialize models into tables, views, and other options, and write them into BigQuery (as well as Redshift and others).
    - By using this tool, we are aiming to remove siloed knowledge and to move toward a more flexible BI tool, that allows us to migrate the business logic, at whichever time, to whichever third-party tool we find best suits our needs.

- **Best Practices when using dbt** - *as of the 20th of November, 2019*:
    - Limit references to raw data
    - Rename and recast fields once
    - Group your models in directories
    - Add tests to your models
    - Consider the information architecture of your data warehouse
    - Separate source-centric and business-centric transformations


 - Company Style Guide
 - FAQS
 - Reports
 - **Contact Person:** Enrique Colin (enrique.colin@ebury.com)


{% enddocs %}
