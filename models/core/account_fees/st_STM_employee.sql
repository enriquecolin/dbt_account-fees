
WITH
  sales AS(
  SELECT
    *
  FROM
    {{ref('stg_account_fees__sales_start_dates')}}
  ),
  factinvoicesplit AS(
  SELECT
    *
  FROM
    {{ref('factinvoicesplit')}}
  ),
  final AS(
  SELECT
    factinvoicesplit.* EXCEPT(Amount_split),
    Email AS Email,
    Full_Name AS Full_Name,
    Employee_Country AS Employee_Country,
    CM_Email AS CM_Email
  FROM
    factinvoicesplit
  LEFT JOIN
    sales
  USING
    (BOS_Username)
)
SELECT
  *
FROM
  final
