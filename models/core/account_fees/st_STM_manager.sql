
WITH
  sales AS(
  SELECT
    *
  FROM
    {{ref('stg_account_fees__sales_start_dates')}}
  ),
  factinvoicesplit AS(
  SELECT
    *
  FROM
    {{ref('factinvoicesplit')}}
  ),
  factinvoicesplit_sales AS(
  SELECT
    factinvoicesplit.* EXCEPT(Amount_split),
    Email AS Email,
    Full_Name AS Full_Name,
    Employee_Country AS Employee_Country,
    CM_Country AS CM_Country,
    CM_Email AS CM_Email,
    Team_Lead_Name AS Team_Lead_Name,
    IsActive AS Active
  FROM
    factinvoicesplit
  LEFT JOIN
    sales
  USING
    (BOS_Username)
  ),
  grouped AS(
  SELECT
    CM_Email,
    Full_Name,
    Employee_Country,
    CM_Country,
    Role,
    CF_Role,
    Team_Lead_Name,
    Active,
    CASE
      WHEN CF_Role IN ('FX_CF_Dealer') THEN 'Dealer'
      WHEN CF_Role IN ('FX_CF_PrimarySalesperson', 'FX_CF_SecondarySalesperson',
                       'TF_CF_PrimarySalesperson', 'TF_CF_SecondarySalesperson')
                       THEN 'HOD-SS2'
      WHEN CF_Role IN ('FX_CF_LeadSourcer', 'TF_CF_LeadSourcer') THEN 'BD-SS1'
    END AS Revenue_Allocation,
    Ebury_Country,
    Channel,
    Paid,
    CASE
      WHEN Date >= DATE_SUB(CURRENT_DATE('GMT'), INTERVAL 7 DAY)
            THEN SUM(GBP_Gross_Profit)
      ELSE
        0
    END AS Week_GBP_Gross_Profit,
    CASE
      WHEN (EXTRACT(MONTH FROM CURRENT_DATE('GMT')) = EXTRACT(MONTH FROM Date) AND
            EXTRACT(YEAR FROM CURRENT_DATE('GMT')) = EXTRACT(YEAR FROM Date))
            THEN SUM(GBP_Gross_Profit)
      ELSE
        0
    END AS MTD_GBP_Gross_Profit,
    CASE
      WHEN Date >= '2019-05-01' THEN SUM(GBP_Gross_Profit)
      ELSE
        0
    END AS YTD_GBP_Gross_Profit
  FROM
    factinvoicesplit_sales
  GROUP BY CM_Email, Full_Name, Employee_Country, CM_Country, Team_Lead_Name,
           Active, Role, CF_Role, Ebury_Country, Channel, Paid, Date
  ),
  final AS (
  SELECT
    CM_Email,
    Full_Name,
    Employee_Country,
    CM_Country,
    Role,
    CF_Role,
    Team_Lead_Name,
    Active,
    Revenue_Allocation,
    Ebury_Country,
    Channel,
    Paid,
    SUM(Week_GBP_Gross_Profit) AS Week_GBP_Gross_Profit,
    SUM(MTD_GBP_Gross_Profit) AS MTD_GBP_Gross_Profit,
    SUM(YTD_GBP_Gross_Profit) AS YTD_GBP_Gross_Profit
  FROM
    grouped
  GROUP BY
    CM_Email, Full_Name, Employee_Country, CM_Country, Team_Lead_Name,
    Active, Role, CF_Role, Revenue_Allocation, Ebury_Country, Channel, Paid
)
SELECT
  *
FROM
  final
