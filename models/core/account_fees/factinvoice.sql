
WITH
  invoices AS(
  SELECT
    *
  FROM
    {{ref('stg_account_fees__invoices')}}),
  account_last AS(
  SELECT
    *
  FROM
    {{ref('stg_account_fees__account_last')}}),
  final AS (
  SELECT
    Document_Number,
    Account_Number,
    Ebury_Country,
    Date,
    Date_Closed,
    Amount
  FROM
    invoices
  LEFT JOIN
    account_last
  USING
    (Account_Number)
  WHERE
    Account_Number IS NOT NULL
  )
SELECT
  *
FROM
  final
