
WITH
  invoices_with_account_hist AS(
  SELECT
    *
  FROM
    {{ref('invoices_with_account_hist')}}),
  account_last AS(
  SELECT
    *
  FROM
    {{ref('stg_account_fees__account_last')}}),
  final AS (
  SELECT
    Document_Number,
    Account_Number,
    Ebury_Country,
    IF(Ebury_Office = 'Solutions', 'Indirect', 'Direct') AS Channel,
    Date,
    Date_Closed,
    Amount,
    IFNULL(CAST(First_Trade_Date AS DATE), First_Invoice_Date) AS First_Trade_Date,
    First_Invoice_Date,
    IFNULL(FX_Dealer_hist,
      FX_Dealer) AS FX_Dealer,
    IFNULL(FX_Primary_Salesperson_hist,
      FX_Primary_Salesperson) AS FX_Primary_Salesperson,
    IFNULL(FX_Secondary_Salesperson_hist,
      FX_Secondary_Salesperson) AS FX_Secondary_Salesperson,
    IFNULL(FX_Lead_Sourcer_hist,
      FX_Lead_Sourcer) AS FX_Lead_Sourcer,
    IFNULL(TF_Primary_Salesperson_hist,
      TF_Primary_Salesperson) AS TF_Primary_Salesperson,
    IFNULL(TF_Secondary_Salesperson_hist,
      TF_Secondary_Salesperson) AS TF_Secondary_Salesperson,
    IFNULL(TF_Lead_Sourcer_hist,
      TF_Lead_Sourcer) AS TF_Lead_Sourcer
  FROM
    invoices_with_account_hist
  JOIN
    account_last
  USING
    (Account_Number)
  )
SELECT
  *
FROM
  final
