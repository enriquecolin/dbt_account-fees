
WITH
  invoices_users AS(
  SELECT
    *
  FROM
    {{ref('invoices_with_account_hist_and_last')}}),
  sales_tracking AS(
  SELECT
    *
  FROM
    {{ref('stg_account_fees__sales_start_dates')}} ),
  final AS(
  SELECT
    i.*,
    CASE
      WHEN fxd.Current_Job_Title LIKE 'Country Manager' THEN 'CM'
      WHEN i.First_Trade_Date >= fxd.KAD_Start_date AND i.First_Trade_Date <= fxd.KAD_End_date THEN 'KAD'
      WHEN i.First_Trade_Date >= fxd.Dealer_Start_date AND i.First_Trade_Date <= fxd.Dealer_End_date THEN 'Dealer'
      WHEN i.First_Trade_Date >= fxd.HoD_Start_date AND i.First_Trade_Date <= fxd.HoD_End_date THEN 'HoD'
      WHEN i.First_Trade_Date >= fxd.SS2_Start_date AND i.First_Trade_Date <= fxd.SS2_End_date THEN 'SS2'
      WHEN i.First_Trade_Date >= fxd.SS1_Start_date AND i.First_Trade_Date <= fxd.SS1_End_date THEN 'SS1'
      WHEN i.First_Trade_Date >= fxd.BD_Start_date AND i.First_Trade_Date <= fxd.BD_End_date THEN 'BD'
      WHEN i.FX_Dealer IS NOT NULL THEN 'Unknown'
      ELSE '-1'
    END
    AS FX_Dealer_Role,
    CASE
      WHEN fxps.Current_Job_Title LIKE 'Country Manager' THEN 'CM'
      WHEN i.First_Trade_Date >= fxps.KAD_Start_date AND i.First_Trade_Date <= fxps.KAD_End_date THEN 'KAD'
      WHEN i.First_Trade_Date >= fxps.Dealer_Start_date AND i.First_Trade_Date <= fxps.Dealer_End_date THEN 'Dealer'
      WHEN i.First_Trade_Date >= fxps.HoD_Start_date AND i.First_Trade_Date <= fxps.HoD_End_date THEN 'HoD'
      WHEN i.First_Trade_Date >= fxps.SS2_Start_date AND i.First_Trade_Date <= fxps.SS2_End_date THEN 'SS2'
      WHEN i.First_Trade_Date >= fxps.SS1_Start_date AND i.First_Trade_Date <= fxps.SS1_End_date THEN 'SS1'
      WHEN i.First_Trade_Date >= fxps.BD_Start_date AND i.First_Trade_Date <= fxps.BD_End_date THEN 'BD'
      WHEN i.FX_Primary_Salesperson IS NOT NULL THEN 'Unknown'
      ELSE '-1'
    END
    AS FX_Primary_Salesperson_Role,
    CASE
      WHEN fxss.Current_Job_Title LIKE 'Country Manager' THEN 'CM'
      WHEN i.First_Trade_Date >= fxss.KAD_Start_date AND i.First_Trade_Date <= fxss.KAD_End_date THEN 'KAD'
      WHEN i.First_Trade_Date >= fxss.Dealer_Start_date AND i.First_Trade_Date <= fxss.Dealer_End_date THEN 'Dealer'
      WHEN i.First_Trade_Date >= fxss.HoD_Start_date AND i.First_Trade_Date <= fxss.HoD_End_date THEN 'HoD'
      WHEN i.First_Trade_Date >= fxss.SS2_Start_date AND i.First_Trade_Date <= fxss.SS2_End_date THEN 'SS2'
      WHEN i.First_Trade_Date >= fxss.SS1_Start_date AND i.First_Trade_Date <= fxss.SS1_End_date THEN 'SS1'
      WHEN i.First_Trade_Date >= fxss.BD_Start_date AND i.First_Trade_Date <= fxss.BD_End_date THEN 'BD'
      WHEN i.FX_Secondary_Salesperson IS NOT NULL THEN 'Unknown'
      ELSE '-1'
    END
    AS FX_Secondary_Salesperson_Role,
    CASE
      WHEN fxls.Current_Job_Title LIKE 'Country Manager' THEN 'CM'
      WHEN i.First_Trade_Date >= fxls.KAD_Start_date AND i.First_Trade_Date <= fxls.KAD_End_date THEN 'KAD'
      WHEN i.First_Trade_Date >= fxss.HoD_Start_date AND i.First_Trade_Date <= fxls.HoD_End_date THEN 'HoD'
      WHEN i.First_Trade_Date >= fxls.Dealer_Start_date AND i.First_Trade_Date <= fxls.Dealer_End_date THEN 'Dealer'
      WHEN i.First_Trade_Date >= fxls.SS2_Start_date AND i.First_Trade_Date <= fxls.SS2_End_date THEN 'SS2'
      WHEN i.First_Trade_Date >= fxls.SS1_Start_date AND i.First_Trade_Date <= fxls.SS1_End_date THEN 'SS1'
      WHEN i.First_Trade_Date >= fxls.BD_Start_date AND i.First_Trade_Date <= fxls.BD_End_date THEN 'BD'
      WHEN i.FX_Lead_Sourcer IS NOT NULL THEN 'Unknown'
      ELSE '-1'
    END
    AS FX_Lead_Sourcer_Role,
    CASE
      WHEN tfps.Current_Job_Title LIKE 'Country Manager' THEN 'CM'
      WHEN i.First_Trade_Date >= tfps.KAD_Start_date AND i.First_Trade_Date <= tfps.KAD_End_date THEN 'KAD'
      WHEN i.First_Trade_Date >= tfps.SS2_Start_date AND i.First_Trade_Date <= tfps.SS2_End_date THEN 'SS2'
      WHEN i.First_Trade_Date >= tfps.HoD_Start_date AND i.First_Trade_Date <= tfps.HoD_End_date THEN 'HoD'
      WHEN i.First_Trade_Date >= tfps.Dealer_Start_date AND i.First_Trade_Date <= tfps.Dealer_End_date THEN 'Dealer'
      WHEN i.First_Trade_Date >= tfps.SS1_Start_date AND i.First_Trade_Date <= tfps.SS1_End_date THEN 'SS1'
      WHEN i.First_Trade_Date >= tfps.BD_Start_date AND i.First_Trade_Date <= tfps.BD_End_date THEN 'BD'
      WHEN i.TF_Primary_Salesperson IS NOT NULL THEN 'Unknown'
      ELSE '-1'
    END
    AS TF_Primary_Salesperson_Role,
    CASE
      WHEN tfss.Current_Job_Title LIKE 'Country Manager' THEN 'CM'
      WHEN i.First_Trade_Date >= tfss.KAD_Start_date AND i.First_Trade_Date <= tfss.KAD_End_date THEN 'KAD'
      WHEN i.First_Trade_Date >= tfss.SS2_Start_date AND i.First_Trade_Date <= tfss.SS2_End_date THEN 'SS2'
      WHEN i.First_Trade_Date >= tfss.HoD_Start_date AND i.First_Trade_Date <= tfss.HoD_End_date THEN 'HoD'
      WHEN i.First_Trade_Date >= tfss.Dealer_Start_date AND i.First_Trade_Date <= tfss.Dealer_End_date THEN 'Dealer'
      WHEN i.First_Trade_Date >= tfss.SS1_Start_date AND i.First_Trade_Date <= tfss.SS1_End_date THEN 'SS1'
      WHEN i.First_Trade_Date >= tfss.BD_Start_date AND i.First_Trade_Date <= tfss.BD_End_date THEN 'BD'
      WHEN i.TF_Secondary_Salesperson IS NOT NULL THEN 'Unknown'
      ELSE '-1'
    END
    AS TF_Secondary_Salesperson_Role,
    CASE
      WHEN tfls.Current_Job_Title LIKE 'Country Manager' THEN 'CM'
      WHEN i.First_Trade_Date >= tfls.KAD_Start_date AND i.First_Trade_Date <= tfls.KAD_End_date THEN 'KAD'
      WHEN i.First_Trade_Date >= tfls.SS2_Start_date AND i.First_Trade_Date <= tfls.SS2_End_date THEN 'SS2'
      WHEN i.First_Trade_Date >= tfls.HoD_Start_date AND i.First_Trade_Date <= tfls.HoD_End_date THEN 'HoD'
      WHEN i.First_Trade_Date >= tfls.Dealer_Start_date AND i.First_Trade_Date <= tfls.Dealer_End_date THEN 'Dealer'
      WHEN i.First_Trade_Date >= tfls.SS1_Start_date AND i.First_Trade_Date <= tfls.SS1_End_date THEN 'SS1'
      WHEN i.First_Trade_Date >= tfls.BD_Start_date AND i.First_Trade_Date <= tfls.BD_End_date THEN 'BD'
      WHEN i.TF_Lead_Sourcer IS NOT NULL THEN 'Unknown'
      ELSE '-1'
    END
    AS TF_Lead_Sourcer_Role
  FROM
    invoices_users i
  LEFT JOIN
    sales_tracking fxd
  ON
    i.FX_Dealer = fxd.BOS_Username
  LEFT JOIN
    sales_tracking fxps
  ON
    i.FX_Primary_Salesperson = fxps.BOS_Username
  LEFT JOIN
    sales_tracking fxss
  ON
    i.FX_Secondary_Salesperson = fxss.BOS_Username
  LEFT JOIN
    sales_tracking fxls
  ON
    i.FX_Lead_Sourcer = fxls.BOS_Username
  LEFT JOIN
    sales_tracking tfps
  ON
    i.TF_Primary_Salesperson = tfps.BOS_Username
  LEFT JOIN
    sales_tracking tfss
  ON
    i.TF_Secondary_Salesperson = tfss.BOS_Username
  LEFT JOIN
    sales_tracking tfls
  ON
    i.TF_Lead_Sourcer = tfls.BOS_Username
  )
SELECT
  *
FROM
  final
