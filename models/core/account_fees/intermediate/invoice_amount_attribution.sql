-- If lead sourcer is missing I need to allocate revenues to an unknown user
-- with unknown office
WITH
  sales_attrib AS(
  SELECT
    *
  FROM
    {{ref('stg_account_fees__sales_attribution_scenarios')}}
  WHERE
    Type='SS2/HOD/KAD/DL/CM'),
  sourcer_attrib AS(
  SELECT
    *
  FROM
    {{ref('stg_account_fees__sales_attribution_scenarios')}}
  WHERE
    Type='BD/SS1'),
  dealer_attrib AS(
  SELECT
    *
  FROM
    {{ref('stg_account_fees__sales_attribution_scenarios')}}
  WHERE
    Type='Dealer'),
  fctinv AS(
  SELECT
    *
  FROM
    {{ref('role_mapping')}}
  ),
  final AS(
  SELECT
    fctinv.*,
    fctinv.Amount * sales_attrib.FX_Primary_Salesperson_Share AS FX_Primary_Salesperson_Share,
    fctinv.Amount * sales_attrib.FX_Secondary_Salesperson_Share AS FX_Secondary_Salesperson_Share,
    fctinv.Amount * sales_attrib.TF_Primary_Salesperson_Share AS TF_Primary_Salesperson_Share,
    fctinv.Amount * sales_attrib.TF_Secondary_Salesperson_Share AS TF_Secondary_Salesperson_Share,
    fctinv.Amount * sourcer_attrib.FX_Lead_Sourcer_Share AS FX_Lead_Sourcer_Share,
    fctinv.Amount * sourcer_attrib.TF_Lead_Sourcer_Share AS TF_Lead_Sourcer_Share,
    fctinv.Amount * dealer_attrib.FX_Dealer_Share AS FX_Dealer_Share
  FROM
    fctinv
  LEFT JOIN
    sales_attrib
  ON
    fctinv.FX_Primary_Salesperson_Role_mask = sales_attrib.FX_Primary_Salesperson_mask
    AND fctinv.FX_Secondary_Salesperson_Role_mask = sales_attrib.FX_Secondary_Salesperson_mask
    AND fctinv.TF_Primary_Salesperson_Role_mask = sales_attrib.TF_Primary_Salesperson_mask
    AND fctinv.TF_Secondary_Salesperson_Role_mask = sales_attrib.TF_Secondary_Salesperson_mask
  LEFT JOIN
    sourcer_attrib
  ON
    fctinv.FX_Lead_Sourcer_Role_mask = sourcer_attrib.FX_Lead_Sourcer_mask
    AND fctinv.TF_Lead_Sourcer_Role_mask = sourcer_attrib.TF_Lead_Sourcer_mask
  LEFT JOIN
    dealer_attrib
  ON
    fctinv.FX_Dealer_Role_mask = dealer_attrib.FX_Dealer_mask
  )
SELECT
  *
FROM
  final
