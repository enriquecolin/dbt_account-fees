{% set roles = ['FX_Dealer_Role', 'FX_Primary_Salesperson_Role',
                'FX_Secondary_Salesperson_Role','FX_Lead_Sourcer_Role',
                'TF_Primary_Salesperson_Role', 'TF_Secondary_Salesperson_Role',
                'TF_Lead_Sourcer_Role'] %}

WITH
  sales_roles_first_trade AS(
  SELECT
    *
  FROM
    {{ref('sales_roles_first_trade')}} ),
  final AS(
  SELECT
    *,

    {% for role in roles -%}

    CASE
      WHEN {{role}} NOT LIKE '-1' THEN 'Role'
      ELSE {{role}}
    END
    AS {{role}}_mask,

    {% endfor -%}

    'dummy' AS dummy
  FROM
    sales_roles_first_trade
  )
SELECT
  * EXCEPT(dummy)
FROM
  final
