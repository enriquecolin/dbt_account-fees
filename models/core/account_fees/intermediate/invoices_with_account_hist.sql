
WITH
  invoices AS(
  SELECT
    *
  FROM
    {{ref('stg_account_fees__invoices')}}),
  account_history AS(
  SELECT
    *
  FROM
    {{ref('stg_account_fees__account_history')}}),
  final AS (
  SELECT
    Document_Number,
    Account_Number,
    Date,
    Date_Closed,
    Amount,
    First_Invoice_Date,
    MAX(FX_Dealer_hist) AS FX_Dealer_hist,
    MAX(FX_Primary_Salesperson_hist) AS FX_Primary_Salesperson_hist,
    MAX(FX_Secondary_Salesperson_hist) AS FX_Secondary_Salesperson_hist,
    MAX(FX_Lead_Sourcer_hist) AS FX_Lead_Sourcer_hist,
    MAX(TF_Primary_Salesperson_hist) AS TF_Primary_Salesperson_hist,
    MAX(TF_Secondary_Salesperson_hist) AS TF_Secondary_Salesperson_hist,
    MAX(TF_Lead_Sourcer_hist) AS TF_Lead_Sourcer_hist
  FROM (
    SELECT
      invoices.Document_Number,
      invoices.Account_Number,
      invoices.Date,
      invoices.Date_Closed,
      invoices.Amount,
      min(invoices.Date) OVER (PARTITION BY invoices.Account_Number) AS First_Invoice_Date,
    IF
      ((account_history.ChangedField = 'Dealer__c')
        AND (invoices.Date >= account_history.Date_From)
        AND (invoices.Date < IFNULL(account_history.Date_To,
            '2199-01-01')),
        account_history.BOS_Username,
        NULL) AS FX_Dealer_hist,
    IF
      ((account_history.ChangedField = 'Sales_Exec__c')
        AND (invoices.Date >= account_history.Date_From)
        AND (invoices.Date < IFNULL(account_history.Date_To,
            '2199-01-01')),
        account_history.BOS_Username,
        NULL) AS FX_Primary_Salesperson_hist,
    IF
      ((account_history.ChangedField = 'Second_Sales_Executive__c')
        AND (invoices.Date >= account_history.Date_From)
        AND (invoices.Date < IFNULL(account_history.Date_To,
            '2199-01-01')),
        account_history.BOS_Username,
        NULL) AS FX_Secondary_Salesperson_hist,
    IF
      ((account_history.ChangedField = 'Lead_Sourcer__c')
        AND (invoices.Date >= account_history.Date_From)
        AND (invoices.Date < IFNULL(account_history.Date_To,
            '2199-01-01')),
        account_history.BOS_Username,
        NULL) AS FX_Lead_Sourcer_hist,
    IF
      ((account_history.ChangedField = 'TF_Primary_Salesperson__c')
        AND (invoices.Date >= account_history.Date_From)
        AND (invoices.Date < IFNULL(account_history.Date_To,
            '2199-01-01')),
        account_history.BOS_Username,
        NULL) AS TF_Primary_Salesperson_hist,
    IF
      ((account_history.ChangedField = 'TF_Secondary_Salesperson__c')
        AND (invoices.Date >= account_history.Date_From)
        AND (invoices.Date < IFNULL(account_history.Date_To,
            '2199-01-01')),
        account_history.BOS_Username,
        NULL) AS TF_Secondary_Salesperson_hist,
    IF
      ((account_history.ChangedField = 'TF_Lead_Sourcer__c')
        AND (invoices.Date >= account_history.Date_From)
        AND (invoices.Date < IFNULL(account_history.Date_To,
            '2199-01-01')),
        account_history.BOS_Username,
        NULL) AS TF_Lead_Sourcer_hist
    FROM
      invoices
    LEFT JOIN
      account_history
    USING
      (Account_Number))
  GROUP BY
    Document_Number,
    Account_Number,
    Date,
    Date_Closed,
    Amount,
    First_Invoice_Date
  )
SELECT
  *
FROM
  final
