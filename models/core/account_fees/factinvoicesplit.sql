
WITH
  invoice_attrib AS(
  SELECT
    *
  FROM
    {{ref('invoice_amount_attribution')}}
  ),
  factinvoicesplit AS(
  SELECT
    Document_Number,
    Account_Number,
    Ebury_Country,
    Channel,
    Date,
    Date_Closed,
    Amount,
    'FX_CF_Dealer' AS CF_Role,
    FX_Dealer AS BOS_Username,
    IF(FX_Dealer_Role IN ('Dealer'), FX_Dealer_Role, 'Unknown') AS Role,
    FX_Dealer_Share AS Amount_split
  FROM
    invoice_attrib
  UNION ALL
  SELECT
    Document_Number,
    Account_Number,
    Ebury_Country,
    Channel,
    Date,
    Date_Closed,
    Amount,
    'FX_CF_LeadSourcer' AS CF_Role,
    FX_Lead_Sourcer AS BOS_Username,
    IF(FX_Lead_Sourcer_Role IN ('BD', 'SS1'), FX_Lead_Sourcer_Role, 'Unknown') AS Role,
    FX_Lead_Sourcer_Share AS Amount_split
  FROM
    invoice_attrib
  UNION ALL
  SELECT
    Document_Number,
    Account_Number,
    Ebury_Country,
    Channel,
    Date,
    Date_Closed,
    Amount,
    'TF_CF_LeadSourcer' AS CF_Role,
    TF_Lead_Sourcer AS BOS_Username,
    IF(TF_Lead_Sourcer_Role IN ('BD', 'SS1'), TF_Lead_Sourcer_Role, 'Unknown') AS Role,
    TF_Lead_Sourcer_Share AS Amount_split
  FROM
    invoice_attrib
  UNION ALL
  SELECT
    Document_Number,
    Account_Number,
    Ebury_Country,
    Channel,
    Date,
    Date_Closed,
    Amount,
    'FX_CF_PrimarySalesperson' AS CF_Role,
    FX_Primary_Salesperson AS BOS_Username,
    IF(FX_Primary_Salesperson_Role IN ('SS2', 'HoD', 'KAD', 'Dealer', 'CM'), FX_Primary_Salesperson_Role, 'Unknown') AS Role,
    FX_Primary_Salesperson_Share AS Amount_split
  FROM
    invoice_attrib
  UNION ALL
  SELECT
    Document_Number,
    Account_Number,
    Ebury_Country,
    Channel,
    Date,
    Date_Closed,
    Amount,
    'TF_CF_PrimarySalesperson' AS CF_Role,
    TF_Primary_Salesperson AS BOS_Username,
    IF(TF_Primary_Salesperson_Role IN ('SS2', 'HoD', 'KAD', 'Dealer', 'CM'), TF_Primary_Salesperson_Role, 'Unknown') AS Role,
    TF_Primary_Salesperson_Share AS Amount_split
  FROM
    invoice_attrib
  UNION ALL
  SELECT
    Document_Number,
    Account_Number,
    Ebury_Country,
    Channel,
    Date,
    Date_Closed,
    Amount,
    'FX_CF_SecondarySalesperson' AS CF_Role,
    FX_Secondary_Salesperson AS BOS_Username,
    IF(FX_Secondary_Salesperson_Role IN ('SS2', 'HoD', 'KAD', 'Dealer', 'CM'), FX_Secondary_Salesperson_Role, 'Unknown') AS Role,
    FX_Secondary_Salesperson_Share AS Amount_split
  FROM
    invoice_attrib
  UNION ALL
  SELECT
    Document_Number,
    Account_Number,
    Ebury_Country,
    Channel,
    Date,
    Date_Closed,
    Amount,
    'TF_CF_SecondarySalesperson' AS CF_Role,
    TF_Secondary_Salesperson AS BOS_Username,
    IF(TF_Secondary_Salesperson_Role IN ('SS2', 'HoD', 'KAD', 'Dealer', 'CM'), TF_Secondary_Salesperson_Role, 'Unknown') AS Role,
    TF_Secondary_Salesperson_Share AS Amount_split
  FROM
    invoice_attrib
  ORDER BY Document_Number
  ),
  final AS(
  SELECT
    factinvoicesplit.*,
    IF(Date_Closed IS NULL, 0, 1) AS Paid,
    IFNULL(Amount_split, 0) AS GBP_Gross_Profit
  FROM
    factinvoicesplit
)
SELECT
  *
FROM
  final
