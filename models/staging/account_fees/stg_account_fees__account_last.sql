
WITH
  source AS (
  SELECT
    *
  FROM
  {{source('account_fees_seed_data', 'account_last')}}),
  renamed AS (
    SELECT
      *
    FROM
      source)
SELECT
  *
FROM
  renamed
