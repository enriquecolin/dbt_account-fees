
WITH
  source AS (
  SELECT
    *
  FROM
  {{source('account_fees_seed_data', 'invoices')}}),
  renamed AS (
    SELECT
      * EXCEPT(Entity__BOS_ID),
      CAST(IFNULL(Entity__BOS_ID, -1) AS STRING) AS Account_Number
    FROM
      source)
SELECT
  *
FROM
  renamed
