
WITH
  source AS (
  SELECT
    *
  FROM
  {{source('account_fees_seed_data', 'sales_attribution_scenarios')}}),
  renamed AS (
  SELECT
    Type,
    Scenario,
    IF(FX_Primary_Salesperson IS NULL, '-1', 'Role') AS FX_Primary_Salesperson_mask,
    IF(FX_Secondary_Salesperson IS NULL, '-1', 'Role') AS FX_Secondary_Salesperson_mask,
    IF(TF_Primary_Salesperson IS NULL, '-1', 'Role') AS TF_Primary_Salesperson_mask,
    IF(TF_Secondary_Salesperson IS NULL, '-1', 'Role') AS TF_Secondary_Salesperson_mask,
    IF(FX_Leadsourcer IS NULL, '-1', 'Role') AS FX_Lead_Sourcer_mask,
    IF(TF_Leadsourcer IS NULL, '-1', 'Role') AS TF_Lead_Sourcer_mask,
    IF(FX_Dealer IS NULL, '-1', 'Role') AS FX_Dealer_mask,
    IFNULL(FX_Primary_Salesperson_Share, 0) AS FX_Primary_Salesperson_Share,
    IFNULL(FX_Secondary_Salesperson_Share, 0) AS FX_Secondary_Salesperson_Share,
    IFNULL(TF_Primary_Salesperson_Share, 0) AS TF_Primary_Salesperson_Share,
    IFNULL(TF_Secondary_Salesperson_Share, 0) AS TF_Secondary_Salesperson_Share,
    IFNULL(FX_Leadsourcer_Share, 0) AS FX_Lead_Sourcer_Share,
    IFNULL(TF_Leadsourcer_Share, 0) AS TF_Lead_Sourcer_Share,
    IFNULL(FX_Dealer_Share, 0) AS FX_Dealer_Share
  FROM
    source)
SELECT
  *
FROM
  renamed
