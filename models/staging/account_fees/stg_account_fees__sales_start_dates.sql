
WITH
  source AS (
  SELECT
    *
  FROM
  {{source('account_fees_seed_data', 'sales_start_dates')}}),
  renamed AS (
    SELECT
      BOS_Username,
      Email,
      Full_Name,
      Employee_Country,
      CM_Country,
      CM_Email,
      Team_Lead_Name,
      Current_Job_Title,
      IsActive,
      CAST(Dealer_Start_date AS DATE) AS Dealer_Start_date,
      CAST(Dealer_End_date AS DATE) AS Dealer_End_date,
      CAST(HoD_Start_date AS DATE) AS HoD_Start_date,
      CAST(HoD_End_date AS DATE) AS HoD_End_date,
      CAST(KAD_Start_date AS DATE) AS KAD_Start_date,
      CAST(KAD_End_date AS DATE) AS KAD_End_date,
      CAST(SS2_Start_date AS DATE) AS SS2_Start_date,
      CAST(SS2_End_date AS DATE) AS SS2_End_date,
      CAST(SS1_Start_date AS DATE) AS SS1_Start_date,
      CAST(SS1_End_date AS DATE) AS SS1_End_date,
      CAST(BD_Start_date AS DATE) AS BD_Start_date,
      CAST(BD_End_date AS DATE) AS BD_End_date
    FROM
      source)
SELECT
  *
FROM
  renamed
