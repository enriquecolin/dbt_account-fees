
WITH
  source AS (
  SELECT
    *
  FROM
  {{source('account_fees_seed_data', 'account_history')}}),
  renamed AS (
    SELECT
      * EXCEPT(Date_From,
        Date_To),
      CAST(Date_From AS DATE) AS Date_From,
      CAST(Date_To AS DATE) AS Date_To
    FROM
      source)
SELECT
  *
FROM
  renamed
