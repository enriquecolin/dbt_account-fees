#!/bin/bash
# Upload account_history.csv to GStorage
gsutil cp account_history.csv gs://account_fees_inputs

# Copy table to BQ
bq load --source_format=CSV \
--skip_leading_rows=1 \
--replace account_fees_seed_data.account_history \
gs://account_fees_inputs/account_history.csv account_history.json
